import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Navigator} from './src/navigator/Navigator';
import {AuthProvider} from './src/context/AuthContext';
import {PacientesProvider} from './src/context/PacientesContext';
import {LaboratoriosProvider} from './src/context/LaboratoriosContext';
import {EvaluacionesProvider} from './src/context/EvaluacionesContext';
import {TratamientosProvider} from './src/context/TratamientosContext';
// import {AntecedentesProvider} from './src/context/AntecedentesContext';

const AppState = ({children}: any) => {
  return (
    <AuthProvider>
      <PacientesProvider>
        <LaboratoriosProvider>
          <EvaluacionesProvider>
            <TratamientosProvider>{children}</TratamientosProvider>
          </EvaluacionesProvider>
        </LaboratoriosProvider>
      </PacientesProvider>
    </AuthProvider>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <AppState>
        <Navigator />
      </AppState>
    </NavigationContainer>
  );
};

export default App;
