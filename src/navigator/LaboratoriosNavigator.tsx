import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {LaboratoriosScreen} from '../screens/LaboratoriosScreen';
import {LaboratorioScreen} from '../screens/LaboratorioScreen';

export type LaboratoriosStackParams = {
  LaboratoriosScreen: undefined; //lista de pacientes
  LaboratorioScreen: {paciente_id?: number}; //para nuevo antecedente
};

const Stack = createStackNavigator<LaboratoriosStackParams>();

export const LaboratoriosNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="LaboratoriosScreen"
        component={LaboratoriosScreen}
        options={{title: 'Pacientes para laboratorios'}}
      />
      <Stack.Screen name="LaboratorioScreen" component={LaboratorioScreen} />
    </Stack.Navigator>
  );
};
