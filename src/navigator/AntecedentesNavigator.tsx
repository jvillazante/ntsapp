import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AntecedenteScreen} from '../screens/AntecedenteScreen';
import {AntecedentesScreen} from '../screens/AntecedentesScreen';

export type AntecedentesStackParams = {
  AntecedentesScreen: undefined; //lista de pacientes
  AntecedenteScreen: {paciente_id?: number}; //para nuevo antecedente
};

const Stack = createStackNavigator<AntecedentesStackParams>();

export const AntecedentesNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="AntecedentesScreen"
        component={AntecedentesScreen}
        options={{title: 'Pacientes para evaluacion clinica'}}
      />
      <Stack.Screen name="AntecedenteScreen" component={AntecedenteScreen} />
    </Stack.Navigator>
  );
};
