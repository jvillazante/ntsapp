import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {AuthContext} from '../context/AuthContext';

import {LoginScreen} from '../screens/LoginScreen';
import {ProtectedScreen} from '../screens/ProtectedScreen';
import {LoadingScreen} from '../screens/LoadingScreen';
import {PacientesNavigator} from './PacientesNavigator';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {SeguimientosNavigator} from './SeguimientosNavigator';
// import {DiagnosticosNavigator} from './DiagnosticosNavigator';
// import {AntecedentesNavigator} from './AntecedentesNavigator';
// import {LaboratoriosNavigator} from './LaboratoriosNavigator';
// import {TratamientosNavigator} from './TratamientoNavigator';
// import {DiagnosticosScreen} from '../screens/DiagnosticosScreen';
// import {DiagnosticosNavigator} from './DiagnosticosNavigator';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export const Navigator = () => {
  const {status} = useContext(AuthContext);

  if (status === 'checking') return <LoadingScreen />;

  return (
    <>
      {status !== 'authenticated' ? (
        <>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
              cardStyle: {
                backgroundColor: 'white',
              },
            }}>
            <Stack.Screen name="LoginScreen" component={LoginScreen} />
          </Stack.Navigator>
        </>
      ) : (
        <>
          <Drawer.Navigator>
            <Drawer.Screen
              name="PacienteNavigator"
              component={PacientesNavigator}
              options={{title: 'Lista de pacientes'}}
            />
            {/* <Drawer.Screen
              name="Antecedentes"
              component={AntecedentesNavigator}
              options={{title: 'Evaluacion clinica'}}
            />
            <Drawer.Screen
              name="Laboratorios"
              component={LaboratoriosNavigator}
              options={{title: 'Laboratorios'}}
            />
            <Drawer.Screen
              name="Tratamientos"
              component={TratamientosNavigator}
              options={{title: 'Tratamientos'}}
            /> */}
            <Drawer.Screen
              name="Seguimientos"
              component={SeguimientosNavigator}
              options={{title: 'Seguimientos registrados'}}
            />
            <Drawer.Screen name="Salir" component={ProtectedScreen} />
          </Drawer.Navigator>
        </>
      )}
    </>
  );
};
