import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {EvaluacionesScreen} from '../screens/EvaluacionesScreen';
import {EvaluacionScreen} from '../screens/EvaluacionScreen';

export type EvaluacionesStackParams = {
  EvaluacionesScreen: undefined; //lista de pacientes
  EvaluacionScreen: {paciente_id?: number}; //para nuevo antecedente
};

const Stack = createStackNavigator<EvaluacionesStackParams>();

export const EvaluacionesNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="EvaluacionesScreen"
        component={EvaluacionesScreen}
        options={{title: 'Pacientes para evaluaciones'}}
      />
      <Stack.Screen name="EvaluacionScreen" component={EvaluacionScreen} />
    </Stack.Navigator>
  );
};
