import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {LaboratoriosScreen} from '../screens/LaboratoriosScreen';
import {LaboratorioScreen} from '../screens/LaboratorioScreen';
import {TratamientosScreen} from '../screens/TratamientosScreen';
import {TratamientoScreen} from '../screens/TratamientoScreen';
import {SeguimientosScreen} from '../screens/SeguimientosScreen';
import {SeguimientosOpcionesScreen} from '../screens/SeguimientosOpcionesScreen';
import {EvaluacionScreen} from '../screens/EvaluacionScreen';
import {EvaluacionesScreen} from '../screens/EvaluacionesScreen';

export type AntecedentesStackParams = {
  EvaluacionesScreen: {paciente_id: number};
  EvaluacionScreen: {id?: number};
};

const Stack = createStackNavigator<AntecedentesStackParams>();

export const SeguimientosNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="EvaluacionesScreen"
        component={EvaluacionesScreen}
        options={{title: 'Evaluaciones'}}
      />
      <Stack.Screen name="EvaluacionScreen" component={EvaluacionScreen} />
    </Stack.Navigator>
  );
};
