import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {PacientesScreen} from '../screens/PacientesScreen';
import {EvaluacionesScreen} from '../screens/EvaluacionesScreen';
import {EvaluacionScreen} from '../screens/EvaluacionScreen';
import {LaboratoriosScreen} from '../screens/LaboratoriosScreen';
import {LaboratorioScreen} from '../screens/LaboratorioScreen';
import {TratamientosScreen} from '../screens/TratamientosScreen';
import {TratamientoScreen} from '../screens/TratamientoScreen';
import {SeguimientosScreen} from '../screens/SeguimientosScreen';
import {SeguimientosOpcionesScreen} from '../screens/SeguimientosOpcionesScreen';

export type SeguimientosStackParams = {
  SeguimientosScreen: undefined;
  SeguimientosOpcionesScreen: {paciente_id?: number};
  EvaluacionesScreen: {paciente_id: number};
  EvaluacionScreen: {id?: number};
  LaboratoriosScreen: {paciente_id?: number};
  LaboratorioScreen: {id?: number};
  TratamientosScreen: {paciente_id?: number};
  TratamientoScreen: {id?: number};
};

const Stack = createStackNavigator<SeguimientosStackParams>();

export const SeguimientosNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      {/* <Stack.Screen
        name="PacientesScreen"
        component={PacientesScreen}
        options={{title: 'Pacientes'}}
      /> */}
      <Stack.Screen
        name="SeguimientosScreen"
        component={SeguimientosScreen}
        options={{title: 'Seguimientos'}}
      />
      <Stack.Screen
        name="SeguimientosOpcionesScreen"
        component={SeguimientosOpcionesScreen}
        options={{title: 'Seguimientos opciones'}}
      />
      <Stack.Screen
        name="EvaluacionesScreen"
        component={EvaluacionesScreen}
        options={{title: 'Evaluaciones'}}
      />
      <Stack.Screen name="EvaluacionScreen" component={EvaluacionScreen} />
      <Stack.Screen
        name="LaboratoriosScreen"
        component={LaboratoriosScreen}
        options={{title: 'Laboratorios'}}
      />
      <Stack.Screen name="LaboratorioScreen" component={LaboratorioScreen} />
      <Stack.Screen
        name="TratamientosScreen"
        component={TratamientosScreen}
        options={{title: 'Tratamientos'}}
      />
      <Stack.Screen name="TratamientoScreen" component={TratamientoScreen} />
    </Stack.Navigator>
  );
};
