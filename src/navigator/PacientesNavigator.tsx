import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {PacientesScreen} from '../screens/PacientesScreen';
import {PacienteScreen} from '../screens/PacienteScreen';
import {DiagnosticoScreen} from '../screens/DiagnosticoScreen';

export type PacientesStackParams = {
  PacientesScreen: undefined;
  PacienteScreen: {id?: number; nombre_paciente?: string};
  // DiagnosticoScreen: {id: number};
};

const Stack = createStackNavigator<PacientesStackParams>();

export const PacientesNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="PacientesScreen"
        component={PacientesScreen}
        options={{title: 'Pacientes'}}
      />
      <Stack.Screen
        name="PacienteScreen"
        component={PacienteScreen}
        // options={{title: 'Registrar paciente'}}
      />
      {/* <Stack.Screen
        name="DiagnosticoScreen"
        component={DiagnosticoScreen}
        options={{title: 'Registrar diagnostico'}}
      /> */}
    </Stack.Navigator>
  );
};
