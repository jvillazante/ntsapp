import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {TratamientosScreen} from '../screens/TratamientosScreen';
import {TratamientoScreen} from '../screens/TratamientoScreen';

export type TratamientosStackParams = {
  TratamientosScreen: undefined; //lista de pacientes
  TratamientoScreen: {paciente_id?: number}; //para nuevo antecedente
};

const Stack = createStackNavigator<TratamientosStackParams>();

export const TratamientosNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="TratamientosScreen"
        component={TratamientosScreen}
        options={{title: 'Pacientes para tratamientos'}}
      />
      <Stack.Screen name="TratamientoScreen" component={TratamientoScreen} />
    </Stack.Navigator>
  );
};
