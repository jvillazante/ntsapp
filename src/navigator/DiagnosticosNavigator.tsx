import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {DiagnosticosScreen} from '../screens/DiagnosticosScreen';
import {DiagnosticoScreen} from '../screens/DiagnosticoScreen';

export type DiagnosticosStackParams = {
  DiagnosticosScreen: undefined;
  DiagnosticoScreen: {id?: number};
};

const Stack = createStackNavigator<DiagnosticosStackParams>();

export const DiagnosticosNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Screen
        name="DiagnosticosScreen"
        component={DiagnosticosScreen}
        options={{title: 'Diagnosticos registrados'}}
      />
      <Stack.Screen
        name="DiagnosticoScreen"
        component={DiagnosticoScreen}
        options={{title: 'Datos diagnosticos'}}
      />
    </Stack.Navigator>
  );
};
