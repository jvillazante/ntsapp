export interface TratamientoState {
  errorMessage: string;
}

type TratamientoAction =
  | {type: 'addError'; payload: string}
  | {type: 'removeError'};

export const tratamientosReducer = (
  state: TratamientoState,
  action: TratamientoAction,
): TratamientoState => {
  switch (action.type) {
    case 'addError':
      return {
        ...state,
        errorMessage: action.payload,
      };
    case 'removeError':
      return {
        ...state,
        errorMessage: '',
      };
    default:
      return state;
  }
};
