export interface LaboratorioState {
  errorMessage: string;
}

type LaboratorioAction =
  | {type: 'addError'; payload: string}
  | {type: 'removeError'};

export const laboratoriosReducer = (
  state: LaboratorioState,
  action: LaboratorioAction,
): LaboratorioState => {
  switch (action.type) {
    case 'addError':
      return {
        ...state,
        errorMessage: action.payload,
      };
    case 'removeError':
      return {
        ...state,
        errorMessage: '',
      };
    default:
      return state;
  }
};
