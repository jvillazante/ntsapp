// import React, {createContext, useContext, useEffect, useState} from 'react';
// import ntsApi from '../api/ntsApi';
// import {Paciente, PacientesResponse} from '../interfaces/appInterfaces';
// import {AuthContext} from './AuthContext';
// // import { useState } from 'react';

// type PacientesContextProps = {
//   // errorMessage: string;
//   pacientes: Paciente[];
//   loadPacientes: (
//     sedes_id: number,
//     red_id: number,
//     municipio_id: number,
//     establecimientio_id: number,
//   ) => Promise<void>;
//   addPaciente: (
//     sedes_id: number,
//     red_id: number,
//     municipio_id: number,
//     establecimiento_id: number,
//     tipodocumento_genericoid: number,
//     nrodocumento: string,
//     complemento: string,
//     fechanacimiento: string,
//     esasegurado: number,
//     esverificadosegip: number,
//     esadscrito: number,
//     nombres: string,
//     primerapellido: string,
//     segundoapellido: string,
//     sexo_genericoid: number,
//     correo_electronico: string,
//     celular: string,
//     edad_registro: number,
//     direccion: string,
//     nacionalidad_genericoid: number,
//     user_id: number,
//     persona_id: number,
//   ) => Promise<Paciente>;
//   updatePaciente: (Paciente: Paciente, id: number) => Promise<void>;
//   loadPacienteById: (id: number) => Promise<Paciente>;
// };

// export const PacientesContext = createContext({} as PacientesContextProps);

// export const PacientesProvider = ({children}: any) => {
//   const [pacientes, setPacientes] = useState<Paciente[]>([]);
//   const {user} = useContext(AuthContext);

//   useEffect(() => {
//     loadPacientes(
//       user?.departamento_id,
//       user?.red_id,
//       user?.municipio_id,
//       user?.establecimiento_id,
//     );
//   }, []);

//   const loadPacientes = async (
//     sedes_id: number,
//     red_id: number,
//     municipio_id: number,
//     establecimiento_id: number,
//   ) => {
//     const resp = await ntsApi.get<PacientesResponse>(
//       `/pacientes/${sedes_id}/${red_id}/${municipio_id}/${establecimiento_id}/?limit=10`,
//     );
//     setPacientes([...resp.data.pacientes]);
//   };
//   const addPaciente = async (
//     sedes_id: number,
//     red_id: number,
//     municipio_id: number,
//     establecimiento_id: number,
//     tipodocumento_genericoid: number,
//     nrodocumento: string,
//     complemento: string,
//     fechanacimiento: string,
//     esasegurado: number,
//     esverificadosegip: number,
//     esadscrito: number,
//     nombres: string,
//     primerapellido: string,
//     segundoapellido: string,
//     sexo_genericoid: number,
//     correo_electronico: string,
//     celular: string,
//     edad_registro: number,
//     direccion: string,
//     nacionalidad_genericoid: number,
//     user_id: number,
//     persona_id: number,
//   ): Promise<Paciente> => {
//     // try {
//     const resp = await ntsApi.post<Paciente>('/pacientes', {
//       sedes_id,
//       red_id,
//       municipio_id,
//       establecimiento_id,
//       tipodocumento_genericoid,
//       nrodocumento,
//       complemento,
//       fechanacimiento,
//       esasegurado,
//       esverificadosegip,
//       esadscrito,
//       nombres,
//       primerapellido,
//       segundoapellido,
//       sexo_genericoid,
//       correo_electronico,
//       celular,
//       edad_registro,
//       direccion,
//       nacionalidad_genericoid,
//       user_id,
//       persona_id,
//     });
//     setPacientes([...pacientes, resp.data]);
//     return resp.data;
//     // } catch (error) {
//     //   dispatch({
//     //     type: 'addError',
//     //     payload: error.response.data.msg || 'Información incorrecta',
//     //   });
//     // }
//   };
//   const updatePaciente = async (Paciente: Paciente, id: number) => {};
//   const loadPacienteById = async (id: number): Promise<Paciente> => {
//     const resp = await ntsApi.get<Paciente>(`pacientes/${id}`);
//     return resp.data;
//   };

//   return (
//     <PacientesContext.Provider
//       value={{
//         pacientes,
//         loadPacientes,
//         addPaciente,
//         updatePaciente,
//         loadPacienteById,
//         // loadPacienteCiFecha,
//       }}>
//       {children}
//     </PacientesContext.Provider>
//   );
// };

import React, {createContext, useReducer, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  Antecedente,
  AntecedentesResponse,
} from '../interfaces/antecedentesInterfaces';
import {evaluacionesReducer, EvaluacionState} from './evaluacionesRecuder';

type EvaluacionesContextProps = {
  errorMessage: string;
  evaluaciones: Antecedente[];
  loadEvaluaciones: (paciente_id: number) => Promise<void>;
  addEvaluacion: (
    paciente_cronico_id: number,
    persona_id: number,
    actividad_fisica: number,
    tabaquismo: number,
    alcoholismo: number,
    talla: number,
    peso: number,
    sistolica: number,
    diastolica: number,
    examen_ojo: number,
    cintura: number,
    imc_valor: number,
    imc_descripcion: string,
    fecha_registro: Date,
    user_id: number,
    establecimiento_id: number,
    frutas_verduras: number,
    sal: number,
    alimentos_procesados: number,
    bebidas_azucaradas: number,
    examen_pie: number,
    tipo_g: number,
  ) => Promise<Antecedente>;
  removeError: () => void;
};

const evaluacionInicialState: EvaluacionState = {
  errorMessage: '',
};

export const EvaluacionesContext = createContext(
  {} as EvaluacionesContextProps,
);

export const EvaluacionesProvider = ({children}: any) => {
  const [state, dispatch] = useReducer(
    evaluacionesReducer,
    evaluacionInicialState,
  );

  const [evaluaciones, setEvaluaciones] = useState<Antecedente[]>([]);

  const loadEvaluaciones = async (paciente_id: number) => {
    const resp = await ntsApi.get<AntecedentesResponse>(
      `/antecedentes/${paciente_id}`,
    );
    setEvaluaciones([...resp.data.antecedentes]);
  };

  const addEvaluacion = async (
    paciente_cronico_id: number,
    persona_id: number,
    actividad_fisica: number,
    tabaquismo: number,
    alcoholismo: number,
    talla: number,
    peso: number,
    sistolica: number,
    diastolica: number,
    examen_ojo: number,
    cintura: number,
    imc_valor: number,
    imc_descripcion: string,
    fecha_registro: Date,
    user_id: number,
    establecimiento_id: number,
    frutas_verduras: number,
    sal: number,
    alimentos_procesados: number,
    bebidas_azucaradas: number,
    examen_pie: number,
    tipo_g: number,
  ): Promise<Antecedente> => {
    try {
      const resp = await ntsApi.post<Antecedente>('/antecedentes', {
        paciente_cronico_id,
        persona_id,
        actividad_fisica,
        tabaquismo,
        alcoholismo,
        talla,
        peso,
        sistolica,
        diastolica,
        examen_ojo,
        cintura,
        imc_valor,
        imc_descripcion,
        fecha_registro,
        user_id,
        establecimiento_id,
        frutas_verduras,
        sal,
        alimentos_procesados,
        bebidas_azucaradas,
        examen_pie,
        tipo_g,
      });
      setEvaluaciones([...evaluaciones, resp.data]);
      dispatch({
        type: 'addError',
        payload: 'Se registro correctamente la evaluacion',
      });
      return resp.data;
    } catch (error) {
      dispatch({
        type: 'addError',
        payload:
          error.response.data.errors[0].msg || 'No se puedo guardar los datos',
      });
    }
  };

  const removeError = () => {
    dispatch({type: 'removeError'});
  };

  return (
    <EvaluacionesContext.Provider
      value={{
        ...state,
        evaluaciones,
        loadEvaluaciones,
        addEvaluacion,
        removeError,
      }}>
      {children}
    </EvaluacionesContext.Provider>
  );
};
