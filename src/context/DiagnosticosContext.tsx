import React, {createContext, useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  Diagnostico,
  DiagnosticosResponse,
} from '../interfaces/diagnosticosInterfaces';

type DiagnosticosContextProps = {
  diagnosticos: Diagnostico[];
  loadDiagnosticos: () => Promise<void>;
  addDiagnostico: (
    paciente_cronico_id: number,
    persona_id: number,
    variable_id: number,
    diagnostico_id: string,
    fecha_registro: Date,
    fecha_eliminacion: Date,
    user_id: number,
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimiento_id: number,
    tipo_registro: number,
  ) => Promise<Diagnostico>;
  // updatePaciente: (Paciente: Paciente, id: number) => Promise<void>;
  // loadPacienteCiFecha: (ci: string, fecha: Date) => Promise<Paciente>;
};

export const DiagnosticosContext = createContext(
  {} as DiagnosticosContextProps,
);

export const DiagnosticosProvider = ({children}: any) => {
  const [diagnosticos, setDiagnosticos] = useState<Diagnostico[]>([]);

  useEffect(() => {
    loadDiagnosticos();
  }, []);

  const loadDiagnosticos = async () => {
    const resp = await ntsApi.get<DiagnosticosResponse>(
      '/diagnosticos?limit=10',
    );
    // console.log(resp.data.diagnosticos);

    setDiagnosticos([...resp.data.diagnosticos]);
  };

  const addDiagnostico = async (
    paciente_cronico_id: number,
    persona_id: number,
    variable_id: number,
    diagnostico_id: string,
    fecha_registro: Date,
    fecha_eliminacion: Date,
    user_id: number,
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimiento_id: number,
    tipo_registro: number,
  ): Promise<Diagnostico> => {
    const resp = await ntsApi.post<Diagnostico>('/diagnosticos', {
      paciente_cronico_id,
      persona_id,
      variable_id,
      diagnostico_id,
      fecha_registro,
      fecha_eliminacion,
      user_id,
      sedes_id,
      red_id,
      municipio_id,
      establecimiento_id,
      tipo_registro,
    });
    // console.log(resp.data);

    setDiagnosticos([...diagnosticos, resp.data]);
    return resp.data;
  };
  // const updatePaciente = async (Paciente: Paciente, id: number) => {};
  // const loadPacienteCiFecha = async (ci: string, fecha: Date) => {};

  return (
    <DiagnosticosContext.Provider
      value={{
        diagnosticos,
        loadDiagnosticos,
        addDiagnostico,
        // updatePaciente,
        // loadPacienteCiFecha,
      }}>
      {children}
    </DiagnosticosContext.Provider>
  );
};
