import React, {createContext, useContext, useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  Antecedente,
  // AntecedentesResponse,
} from '../interfaces/antecedentesInterfaces';

type AntecedentesContextProps = {
  addAntecedente: (
    paciente_cronico_id: number,
    persona_id: number,
    actividad_fisica: number,
    tabaquismo: number,
    alcoholismo: number,
    talla: number,
    peso: number,
    sistolica: number,
    diastolica: number,
    examen_ojo: number,
    cintura: number,
    imc_valor: number,
    imc_descripcion: string,
    fecha_registro: string,
    user_id: number,
    establecimiento_id: number,
    frutas_verduras: number,
    sal: number,
    alimentos_procesados: number,
    bebidas_azucaradas: number,
    examen_pie: number,
    tipo_g: number,
  ) => Promise<Antecedente>;
};

export const AntecedentesContext = createContext(
  {} as AntecedentesContextProps,
);

export const AntecedentesProvider = ({children}: any) => {
  const addAntecedente = async (
    paciente_cronico_id: number,
    persona_id: number,
    actividad_fisica: number,
    tabaquismo: number,
    alcoholismo: number,
    talla: number,
    peso: number,
    sistolica: number,
    diastolica: number,
    examen_ojo: number,
    cintura: number,
    imc_valor: number,
    imc_descripcion: string,
    fecha_registro: string,
    user_id: number,
    establecimiento_id: number,
    frutas_verduras: number,
    sal: number,
    alimentos_procesados: number,
    bebidas_azucaradas: number,
    examen_pie: number,
    tipo_g: number,
  ): Promise<Antecedente> => {
    // console.log('llega?');
    const resp = await ntsApi.post<Antecedente>('/antecedentes', {
      paciente_cronico_id,
      persona_id,
      actividad_fisica,
      tabaquismo,
      alcoholismo,
      talla,
      peso,
      sistolica,
      diastolica,
      examen_ojo,
      cintura,
      imc_valor,
      imc_descripcion,
      fecha_registro,
      user_id,
      establecimiento_id,
      frutas_verduras,
      sal,
      alimentos_procesados,
      bebidas_azucaradas,
      examen_pie,
      tipo_g,
    });
    return resp.data;
  };
  return (
    <AntecedentesContext.Provider
      value={{
        addAntecedente,
      }}>
      {children}
    </AntecedentesContext.Provider>
  );
};
