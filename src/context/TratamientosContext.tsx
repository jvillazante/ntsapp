import React, {createContext, useReducer, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  Tratamiento,
  TratamientosResponse,
} from '../interfaces/tratamientosInterfaces';

import {tratamientosReducer, TratamientoState} from './tratamientosReducer';

type TratamientosContextProps = {
  errorMessage: string;
  tratamientos: Tratamiento[];
  loadTratamientos: (paciente_id: number) => Promise<void>;
  addTratamiento: (
    paciente_cronico_id: number,
    persona_id: number,
    enfermedad_id: number,
    medicamento_id: number,
    tratamiento: string,
    fecha_registro: Date,
    user_id: number,
    tipo_g: number,
  ) => Promise<Tratamiento>;
  removeError: () => void;
};

const tratamientoInicialState: TratamientoState = {
  errorMessage: '',
};

export const TratamientosContext = createContext(
  {} as TratamientosContextProps,
);

export const TratamientosProvider = ({children}: any) => {
  const [state, dispatch] = useReducer(
    tratamientosReducer,
    tratamientoInicialState,
  );

  const [tratamientos, setTratamientos] = useState<Tratamiento[]>([]);

  const loadTratamientos = async (paciente_id: number) => {
    const resp = await ntsApi.get<TratamientosResponse>(
      `/tratamientos/${paciente_id}`,
    );
    setTratamientos([...resp.data.tratamientos]);
  };

  const addTratamiento = async (
    paciente_cronico_id: number,
    persona_id: number,
    enfermedad_id: number,
    medicamento_id: number,
    tratamiento: string,
    fecha_registro: Date,
    user_id: number,
    tipo_g: number,
  ): Promise<Tratamiento> => {
    try {
      const resp = await ntsApi.post<Tratamiento>('/tratamientos', {
        paciente_cronico_id,
        persona_id,
        enfermedad_id,
        medicamento_id,
        tratamiento,
        fecha_registro,
        user_id,
        tipo_g,
      });
      setTratamientos([...tratamientos, resp.data]);
      dispatch({
        type: 'addError',
        payload: 'Se registro correctamente el tratamiento',
      });
      return resp.data;
    } catch (error) {
      dispatch({
        type: 'addError',
        payload:
          error.response.data.errors[0].msg || 'No se puedo guardar los datos',
      });
    }
  };

  const removeError = () => {
    dispatch({type: 'removeError'});
  };

  return (
    <TratamientosContext.Provider
      value={{
        ...state,
        tratamientos,
        loadTratamientos,
        addTratamiento,
        removeError,
      }}>
      {children}
    </TratamientosContext.Provider>
  );
};
