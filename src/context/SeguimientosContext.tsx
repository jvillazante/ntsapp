import React, {createContext, useContext, useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {Paciente, PacientesResponse} from '../interfaces/appInterfaces';
import {AuthContext} from './AuthContext';
// import { useState } from 'react';

type SeguimientoContextProps = {
  // errorMessage: string;
  //   pacientes: Paciente[];
  loadPacientes: (
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimientio_id: number,
  ) => Promise<void>;
};

export const SeguimientosContext = createContext({} as SeguimientoContextProps);

export const SeguimientosProvider = ({children}: any) => {
  const [pacientes, setPacientes] = useState<Paciente[]>([]);
  const {user} = useContext(AuthContext);

  useEffect(() => {
    loadPacientes(
      user?.departamento_id,
      user?.red_id,
      user?.municipio_id,
      user?.establecimiento_id,
    );
  }, []);

  const loadPacientes = async (
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimiento_id: number,
  ) => {
    const resp = await ntsApi.get<PacientesResponse>(
      `/pacientes/${sedes_id}/${red_id}/${municipio_id}/${establecimiento_id}/?limit=10`,
    );
    setPacientes([...resp.data.pacientes]);
  };

  return (
    <SeguimientosContext.Provider
      value={{
        loadPacientes,
      }}>
      {children}
    </SeguimientosContext.Provider>
  );
};
