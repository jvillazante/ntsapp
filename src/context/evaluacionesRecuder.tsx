export interface EvaluacionState {
  errorMessage: string;
}

type EvaluacionAction =
  | {type: 'addError'; payload: string}
  | {type: 'removeError'};

export const evaluacionesReducer = (
  state: EvaluacionState,
  action: EvaluacionAction,
): EvaluacionState => {
  switch (action.type) {
    case 'addError':
      return {
        ...state,
        errorMessage: action.payload,
      };
    case 'removeError':
      return {
        ...state,
        errorMessage: '',
      };
    default:
      return state;
  }
};
