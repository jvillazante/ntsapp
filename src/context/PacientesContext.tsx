import React, {createContext, useContext, useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {Paciente, PacientesResponse} from '../interfaces/appInterfaces';
import {AuthContext} from './AuthContext';
// import { useState } from 'react';

type PacientesContextProps = {
  // errorMessage: string;
  pacientes: Paciente[];
  loadPacientes: (
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimientio_id: number,
  ) => Promise<void>;
  addPaciente: (
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimiento_id: number,
    tipodocumento_genericoid: number,
    nrodocumento: string,
    complemento: string,
    fechanacimiento: string,
    esasegurado: number,
    esverificadosegip: number,
    esadscrito: number,
    nombres: string,
    primerapellido: string,
    segundoapellido: string,
    sexo_genericoid: number,
    correo_electronico: string,
    celular: string,
    edad_registro: number,
    direccion: string,
    nacionalidad_genericoid: number,
    user_id: number,
    persona_id: number,
  ) => Promise<Paciente>;
  updatePaciente: (Paciente: Paciente, id: number) => Promise<void>;
  loadPacienteById: (id: number) => Promise<Paciente>;
  // loadPacienteCiFecha: (ci: string, fecha: Date) => Promise<Paciente>;
};

export const PacientesContext = createContext({} as PacientesContextProps);

export const PacientesProvider = ({children}: any) => {
  const [pacientes, setPacientes] = useState<Paciente[]>([]);
  const {user} = useContext(AuthContext);

  useEffect(() => {
    loadPacientes(
      user?.departamento_id,
      user?.red_id,
      user?.municipio_id,
      user?.establecimiento_id,
    );
  }, []);

  const loadPacientes = async (
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimiento_id: number,
  ) => {
    const resp = await ntsApi.get<PacientesResponse>(
      `/pacientes/${sedes_id}/${red_id}/${municipio_id}/${establecimiento_id}/?limit=10`,
    );
    setPacientes([...resp.data.pacientes]);
  };
  const addPaciente = async (
    sedes_id: number,
    red_id: number,
    municipio_id: number,
    establecimiento_id: number,
    tipodocumento_genericoid: number,
    nrodocumento: string,
    complemento: string,
    fechanacimiento: string,
    esasegurado: number,
    esverificadosegip: number,
    esadscrito: number,
    nombres: string,
    primerapellido: string,
    segundoapellido: string,
    sexo_genericoid: number,
    correo_electronico: string,
    celular: string,
    edad_registro: number,
    direccion: string,
    nacionalidad_genericoid: number,
    user_id: number,
    persona_id: number,
  ): Promise<Paciente> => {
    // try {
    const resp = await ntsApi.post<Paciente>('/pacientes', {
      sedes_id,
      red_id,
      municipio_id,
      establecimiento_id,
      tipodocumento_genericoid,
      nrodocumento,
      complemento,
      fechanacimiento,
      esasegurado,
      esverificadosegip,
      esadscrito,
      nombres,
      primerapellido,
      segundoapellido,
      sexo_genericoid,
      correo_electronico,
      celular,
      edad_registro,
      direccion,
      nacionalidad_genericoid,
      user_id,
      persona_id,
    });
    setPacientes([...pacientes, resp.data]);
    return resp.data;
    // } catch (error) {
    //   dispatch({
    //     type: 'addError',
    //     payload: error.response.data.msg || 'Información incorrecta',
    //   });
    // }
  };
  const updatePaciente = async (Paciente: Paciente, id: number) => {};
  const loadPacienteById = async (id: number): Promise<Paciente> => {
    const resp = await ntsApi.get<Paciente>(`pacientes/${id}`);
    return resp.data;
  };

  return (
    <PacientesContext.Provider
      value={{
        pacientes,
        loadPacientes,
        addPaciente,
        updatePaciente,
        loadPacienteById,
        // loadPacienteCiFecha,
      }}>
      {children}
    </PacientesContext.Provider>
  );
};
