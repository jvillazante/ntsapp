import React, {createContext, useReducer, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  LaboratoriosCronico,
  LaboratoriosCronicosResponse,
} from '../interfaces/laboratoriosInterfaces';
import {laboratoriosReducer, LaboratorioState} from './laboratoriosRecuder';

type LaboratoriosContextProps = {
  errorMessage: string;
  laboratorioscronicos: LaboratoriosCronico[];
  loadLaboratorios: (paciente_id: number) => Promise<void>;
  addLaboratorio: (
    paciente_cronico_id: number,
    persona_id: number,
    laboratorio_id: number,
    valor: number,
    fecha_registro: Date,
    user_id: number,
    tipo_g: number,
  ) => Promise<LaboratoriosCronico>;
  removeError: () => void;
};

const laboratorioInicialState: LaboratorioState = {
  errorMessage: '',
};

export const LaboratoriosContext = createContext(
  {} as LaboratoriosContextProps,
);

export const LaboratoriosProvider = ({children}: any) => {
  const [state, dispatch] = useReducer(
    laboratoriosReducer,
    laboratorioInicialState,
  );

  const [laboratorioscronicos, setLaboratorioscronicos] = useState<
    LaboratoriosCronico[]
  >([]);

  const loadLaboratorios = async (paciente_id: number) => {
    const resp = await ntsApi.get<LaboratoriosCronicosResponse>(
      `/laboratorios-cronicos/${paciente_id}`,
    );
    setLaboratorioscronicos([...resp.data.laboratoriosCronicos]);
  };

  const addLaboratorio = async (
    paciente_cronico_id: number,
    persona_id: number,
    laboratorio_id: number,
    valor: number,
    fecha_registro: Date,
    user_id: number,
    tipo_g: number,
  ): Promise<LaboratoriosCronico> => {
    try {
      const resp = await ntsApi.post<LaboratoriosCronico>(
        '/laboratorios-cronicos',
        {
          paciente_cronico_id,
          persona_id,
          laboratorio_id,
          valor,
          fecha_registro,
          user_id,
          tipo_g,
        },
      );
      setLaboratorioscronicos([...laboratorioscronicos, resp.data]);
      dispatch({
        type: 'addError',
        payload: 'Se registro correctamente laboratorio',
      });
      return resp.data;
    } catch (error) {
      dispatch({
        type: 'addError',
        payload:
          error.response.data.errors[0].msg || 'No se puedo guardar los datos',
      });
    }
  };

  const removeError = () => {
    dispatch({type: 'removeError'});
  };

  return (
    <LaboratoriosContext.Provider
      value={{
        ...state,
        laboratorioscronicos,
        loadLaboratorios,
        addLaboratorio,
        removeError,
      }}>
      {children}
    </LaboratoriosContext.Provider>
  );
};
