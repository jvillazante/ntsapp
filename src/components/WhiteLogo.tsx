import React from 'react';
import {Image, View} from 'react-native';

export const WhiteLogo = () => {
  return (
    <View
      style={{
        alignItems: 'center',
      }}>
      <Image
        source={require('../assets/logo.jpeg')}
        style={{
          width: 210,
          height: 200,
        }}
      />
    </View>
  );
};
