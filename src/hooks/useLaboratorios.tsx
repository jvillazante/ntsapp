import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  Laboratorio,
  LaboratoriosResponse,
} from '../interfaces/laboratoriosInterfaces';

export const useLaboratorios = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [laboratorios, setLaboratorios] = useState<Laboratorio[]>([]);

  useEffect(() => {
    getLaboratorios();
  }, []);

  const getLaboratorios = async () => {
    const resp = await ntsApi.get<LaboratoriosResponse>('/laboratorios');
    setLaboratorios(resp.data.laboratorios);
    setIsLoading(false);
  };

  return {
    isLoading,
    laboratorios,
  };
};
