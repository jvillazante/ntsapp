import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {CatalogosResponse, Catalogo} from '../interfaces/catalogosInterfaces';

export const useAlcoholismo = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [alcoholismos, setAlcoholismos] = useState<Catalogo[]>([]);

  useEffect(() => {
    getAlcoholismos();
  }, []);

  const getAlcoholismos = async () => {
    const resp = await ntsApi.get<CatalogosResponse>(`/catalogos/1`);
    setAlcoholismos(resp.data.catalogos);
    setIsLoading(false);
  };

  return {
    isLoading,
    alcoholismos,
  };
};
