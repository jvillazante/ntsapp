import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {CatalogosResponse, Catalogo} from '../interfaces/catalogosInterfaces';

export const useTabaquismo = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [tabaquismos, setTabaquismos] = useState<Catalogo[]>([]);

  useEffect(() => {
    getTabaquismos();
  }, []);

  const getTabaquismos = async () => {
    const resp = await ntsApi.get<CatalogosResponse>(`/catalogos/2`);
    setTabaquismos(resp.data.catalogos);
    setIsLoading(false);
  };

  return {
    isLoading,
    tabaquismos,
  };
};
