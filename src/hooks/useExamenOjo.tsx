import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {CatalogosResponse, Catalogo} from '../interfaces/catalogosInterfaces';

export const useExamenOjo = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [examen_ojos, setExamen_ojos] = useState<Catalogo[]>([]);

  useEffect(() => {
    getExamenes_ojos();
  }, []);

  const getExamenes_ojos = async () => {
    const resp = await ntsApi.get<CatalogosResponse>(`/catalogos/3`);
    setExamen_ojos(resp.data.catalogos);
    setIsLoading(false);
  };

  return {
    isLoading,
    examen_ojos,
  };
};
