import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {MedicamentosResponse} from '../interfaces/medicamentosInterfaces';
import {Medicamento} from '../interfaces/tratamientosInterfaces';

export const useMedicamentosIdVariable = (variable_id: number) => {
  const [isLoading, setIsLoading] = useState(true);
  const [medicamentos, setMedicamentos] = useState<Medicamento[]>([]);

  // useEffect(() => {
  //   getMedicamentos(variable_id);
  // }, []);

  const getMedicamentos = async (variable_id: number) => {
    const resp = await ntsApi.get<MedicamentosResponse>(
      `/medicamentos/${variable_id}`,
    );
    setMedicamentos(resp.data.medicamentos);
    setIsLoading(false);
  };

  return {
    isLoading,
    medicamentos,
  };
};
