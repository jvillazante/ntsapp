import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {
  Variable,
  VariablesResponse,
} from '../interfaces/diagnosticosInterfaces';
// import { CagetoriesResponse, Categoria } from '../interfaces/appInterfaces';

export const useVariables = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [variables, setVariables] = useState<Variable[]>([]);

  useEffect(() => {
    getVariables();
  }, []);

  const getVariables = async () => {
    const resp = await ntsApi.get<VariablesResponse>('/variables');
    setVariables(resp.data.variables);
    setIsLoading(false);
  };

  return {
    isLoading,
    variables,
  };
};

export const useVariablesIdPaciente = (paciente_id: number) => {
  const [isLoading, setIsLoading] = useState(true);
  const [variables, setVariables] = useState<Variable[]>([]);

  useEffect(() => {
    getVariables(paciente_id);
  }, []);

  const getVariables = async (paciente_id: number) => {
    const resp = await ntsApi.get<VariablesResponse>(
      `/variables/${paciente_id}`,
    );
    setVariables(resp.data.variables);
    setIsLoading(false);
  };

  return {
    isLoading,
    variables,
  };
};
