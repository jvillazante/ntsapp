import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {CatalogosResponse, Catalogo} from '../interfaces/catalogosInterfaces';

export const useExamenPie = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [examen_pies, setExamen_pies] = useState<Catalogo[]>([]);

  useEffect(() => {
    getExamenes_pies();
  }, []);

  const getExamenes_pies = async () => {
    const resp = await ntsApi.get<CatalogosResponse>(`/catalogos/4`);
    setExamen_pies(resp.data.catalogos);
    setIsLoading(false);
  };

  return {
    isLoading,
    examen_pies,
  };
};
