import {useEffect, useState} from 'react';
import ntsApi from '../api/ntsApi';
import {Cie10, Cie10Response} from '../interfaces/diagnosticosInterfaces';

export const useCie10 = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [cie10s, setCie10s] = useState<Cie10[]>([]);

  useEffect(() => {
    getCie10s();
  }, []);

  const getCie10s = async () => {
    const resp = await ntsApi.get<Cie10Response>('/cie10s');
    setCie10s(resp.data.cie10s);
    setIsLoading(false);
  };

  return {
    isLoading,
    cie10s,
  };
};
