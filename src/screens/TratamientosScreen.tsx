import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';
import {TratamientosStackParams} from '../navigator/TratamientoNavigator';
import {TratamientosContext} from '../context/TratamientosContext';

interface Props
  extends StackScreenProps<TratamientosStackParams, 'TratamientosScreen'> {}

export const TratamientosScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params;
  const [isRefreshing, setIsRefreshing] = useState(false);
  const {tratamientos, loadTratamientos} = useContext(TratamientosContext);
  // const {user} = useContext(AuthContext);
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          activeOpacity={0.8}
          style={{marginRight: 10}}
          onPress={() =>
            navigation.navigate('TratamientoScreen', {paciente_id})
          }>
          <Text>Nuevo tratamiento </Text>
        </TouchableOpacity>
      ),
    });
  }, []);

  useEffect(() => {
    loadTrata();
  }, []);

  const loadTrata = async () => {
    await loadTratamientos(paciente_id);
  };

  const loadTratamientosCronicosFromBackend = async () => {
    setIsRefreshing(true);
    await loadTratamientos(paciente_id);
    setIsRefreshing(false);
  };

  return (
    <View style={{flex: 1, marginHorizontal: 10}}>
      <FlatList
        data={tratamientos}
        keyExtractor={p => p.uid}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              navigation.navigate('TratamientoScreen', {
                uid: item.uid,
              })
            }>
            <View style={styles.container}>
              <Icon name="person-outline" color="gray" size={20} />
              <Text style={styles.pacienteName}>
                {item.fecha_registro} {'|'}
                {item.variable.descripcion}
                {'|'}
                {item.medicamento.med_comercial}
                {'|'}
                {item.medicamento.med_concentracion}
              </Text>
              <View style={{flex: 1}} />
              <Icon name="chevron-forward-outline" color="gray" size={20} />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={loadTratamientosCronicosFromBackend}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  pacienteName: {
    marginLeft: 10,
    fontSize: 19,
  },
  itemSeparator: {
    borderBottomWidth: 2,
    marginVertical: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
});
