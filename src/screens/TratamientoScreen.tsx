import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
  Alert,
} from 'react-native';

import {StackScreenProps} from '@react-navigation/stack';
import {useForm} from '../hooks/useForm';
import {TratamientosStackParams} from '../navigator/TratamientoNavigator';
import {Picker} from '@react-native-picker/picker';
import {useVariablesIdPaciente} from '../hooks/useVaribles';
// import {useMedicamentosIdVariable} from '../hooks/useMedicamentos';
import ntsApi from '../api/ntsApi';
import {MedicamentosResponse} from '../interfaces/medicamentosInterfaces';
import {TratamientosContext} from '../context/TratamientosContext';
import {AuthContext} from '../context/AuthContext';

interface Props
  extends StackScreenProps<TratamientosStackParams, 'TratamientoScreen'> {}

export const TratamientoScreen = ({navigation, route}: Props) => {
  // const {variables} = useVariables();
  const {paciente_id = 0} = route.params;
  const {user} = useContext(AuthContext);
  const {addTratamiento, loadTratamientos, removeError, errorMessage} =
    useContext(TratamientosContext);
  const {variables} = useVariablesIdPaciente(paciente_id);

  const [medicamentos, setMedicamentos] = useState([]);

  const {enfermedad_id, medicamento_id, tratamiento, onChange, setFormValue} =
    useForm({
      enfermedad_id: 0,
      medicamento_id: 0,
      tratamiento: '',
    });
  useEffect(() => {
    if (errorMessage.length === 0) return;

    Alert.alert('Mensaje importante!', errorMessage, [
      {
        text: 'Ok',
        onPress: removeError,
      },
    ]);
  }, [errorMessage]);

  // useEffect(() => {
  //   loadMedis();
  // }, [enfermedad_id]);

  // const loadMedis = async () => {
  //   const {medicamentos} = useMedicamentosIdVariable(enfermedad_id);
  // };

  useEffect(() => {
    getMedicamentos(enfermedad_id);
  }, [enfermedad_id]);
  const getMedicamentos = async (enfermedad_id: number) => {
    const resp = await ntsApi.get<MedicamentosResponse>(
      `/medicamentos/${enfermedad_id}`,
    );
    setMedicamentos(resp.data.medicamentos);
  };

  const save = async () => {
    const tempEnfermedadId = enfermedad_id || variables[0].id;
    const tempMedicamentoId = medicamento_id || medicamentos[0].id;
    // console.log(paciente_id, 0, tempLaboratorioId, valor, fecha);
    const newTratamiento = await addTratamiento(
      paciente_id,
      0,
      tempEnfermedadId,
      tempMedicamentoId,
      tratamiento,
      new Date(),
      user?.uid,
      2,
    );

    // console.log('Se guardo correctamente!');
    loadTratamientos(paciente_id);
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>Enfermedad</Text>
        <Picker
          selectedValue={enfermedad_id}
          onValueChange={value => onChange(value, 'enfermedad_id')}>
          {variables.map(c => (
            <Picker.Item label={c.descripcion} value={c.uid} key={c.uid} />
          ))}
        </Picker>
        <Text style={styles.label}>Medicamento:</Text>
        <Picker
          selectedValue={medicamento_id}
          onValueChange={value => onChange(value, 'medicamento_id')}>
          {medicamentos.map(c => (
            <Picker.Item label={c.med_comercial} value={c.uid} key={c.uid} />
          ))}
        </Picker>
        <Text style={styles.label}>Frecuencia:</Text>
        <TextInput
          placeholder="Frecuencia"
          style={styles.textInput}
          value={tratamiento}
          onChangeText={value => onChange(value, 'tratamiento')}
        />

        <Button title="Guardar" onPress={save} color="#003d52" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
    marginTop: 5,
    marginBottom: 15,
  },
});
