import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
  Image,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';

import {StackScreenProps} from '@react-navigation/stack';
import {useForm} from '../hooks/useForm';
import {DiagnosticosStackParams} from '../navigator/DiagnosticosNavigator';
// import { DiagnosticosContext } from '../context/DiagnosticosContext';
import {AuthContext} from '../context/AuthContext';
import {useVariables} from '../hooks/useVaribles';
import {useCie10} from '../hooks/useCie10';
import {DiagnosticosContext} from '../context/DiagnosticosContext';

interface Props
  extends StackScreenProps<DiagnosticosStackParams, 'DiagnosticoScreen'> {}

export const DiagnosticoScreen = ({navigation, route}: Props) => {
  const {id = 0} = route.params; //Id del paciente
  const {user} = useContext(AuthContext);

  const {addDiagnostico} = useContext(DiagnosticosContext);

  const {variables} = useVariables();
  const {cie10s} = useCie10();

  const {variable_id, diagnostico_id, fecha, onChange, setFormValue} = useForm({
    variable_id: 0,
    diagnostico_id: '',
    fecha: new Date(),
  });

  const save = async () => {
    const tempVariable_id = variable_id || variables[0].id;
    const tempDiagnostico_id = diagnostico_id || cie10s[0].cie_alfa;
    const newVariable = await addDiagnostico(
      id,
      0,
      tempVariable_id,
      tempDiagnostico_id,
      new Date(),
      fecha,
      user?.uid,
      user?.departamento_id,
      user?.red_id,
      user?.municipio_id,
      user?.establecimiento_id,
      1,
    );
    // console.log(newVariable);
    onChange(newVariable.id, 'uid');
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>Fecha:</Text>
        <TextInput
          placeholder="Fecha"
          style={styles.textInput}
          value={fecha}
          onChangeText={value => onChange(value, 'fecha')}
        />

        {/* Picker / Selector */}
        <Text style={styles.label}>Variable:</Text>
        <Picker
          selectedValue={variable_id}
          onValueChange={value => onChange(value, 'variable_id')}>
          {variables.map(c => (
            <Picker.Item label={c.descripcion} value={c.id} key={c.id} />
          ))}
        </Picker>

        {/* Picker / Selector */}
        <Text style={styles.label}>Cie10:</Text>
        <Picker
          selectedValue={diagnostico_id}
          onValueChange={value => onChange(value, 'diagnostico_id')}>
          {cie10s.map(c => (
            <Picker.Item
              label={c.cie_descripcion}
              value={c.cie_alfa}
              key={c.cie_alfa}
            />
          ))}
        </Picker>

        <Button title="Guardar" onPress={save} color="#003d52" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
    marginTop: 5,
    marginBottom: 15,
  },
});
