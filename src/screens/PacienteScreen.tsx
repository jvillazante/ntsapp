import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';

import {StackScreenProps} from '@react-navigation/stack';
import {useForm} from '../hooks/useForm';
import {AuthContext} from '../context/AuthContext';
import {PacientesStackParams} from '../navigator/PacientesNavigator';
import {PacientesContext} from '../context/PacientesContext';

interface Props
  extends StackScreenProps<PacientesStackParams, 'PacienteScreen'> {}

export const PacienteScreen = ({navigation, route}: Props) => {
  // const {errorMessage, removeError} = useContext(AuthContext);
  const {id = 0, nombre_paciente = ''} = route.params; //Id del paciente
  const {user} = useContext(AuthContext);
  const {addPaciente, loadPacienteById} = useContext(PacientesContext);

  const {
    nrodocumento,
    fechanacimiento,
    nombres,
    primerapellido,
    segundoapellido,
    sexo_genericoid,
    correo_electronico,
    direccion,
    celular,
    onChange,
    setFormValue,
  } = useForm({
    nrodocumento: '',
    fechanacimiento: '',
    nombres: nombre_paciente,
    primerapellido: '',
    segundoapellido: '',
    sexo_genericoid: 0,
    correo_electronico: '',
    direccion: '',
    celular: '',
  });

  useEffect(() => {
    navigation.setOptions({
      title: nombres ? nombres : 'Nombre de paciente',
    });
  }, [nombres]);

  useEffect(() => {
    loadPaciente();
  }, []);

  const loadPaciente = async () => {
    if (id === 0) return;
    const paciente = await loadPacienteById(id);
    setFormValue({
      id,
      nrodocumento: paciente.nrodocumento,
      fechanacimiento: paciente.fechanacimiento,
      nombres: paciente.nombres,
      primerapellido: paciente.primerapellido,
      segundoapellido: paciente.segundoapellido,
      sexo_genericoid: paciente.sexo_genericoid,
      correo_electronico: paciente.correo_electronico,
      celular: paciente.celular,
      nombres: paciente.nombres,
    });
  };

  const save = async () => {
    // console.log('guardar paciente nuevo');
    if (id > 0) {
      // updateProduct(categoriaId, nombre, id);
      console.log('Actualizar producto');
    } else {
      const tempSexo = sexo_genericoid; // || categorsies[0]._id;
      const newPaciente = await addPaciente(
        user?.departamento_id,
        user?.red_id,
        user?.municipio_id,
        user?.establecimiento_id,
        3,
        nrodocumento,
        '',
        fechanacimiento,
        0,
        0,
        0,
        nombres,
        primerapellido,
        segundoapellido,
        sexo_genericoid,
        correo_electronico,
        celular,
        0,
        direccion,
        12, //nacionalidad
        user?.uid,
        0,
      );
      // onChange(newPaciente._id, '_id');
    }
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>CI:</Text>
        <TextInput
          placeholder="Nro documento"
          style={styles.textInput}
          value={nrodocumento}
          onChangeText={value => onChange(value, 'nrodocumento')}
        />
        <Text style={styles.label}>Fecha nacimiento:</Text>
        <TextInput
          placeholder="Fecha"
          style={styles.textInput}
          value={fechanacimiento}
          onChangeText={value => onChange(value, 'fechanacimiento')}
        />
        <Text style={styles.label}>Primer apellido:</Text>
        <TextInput
          placeholder="Primer apellido"
          style={styles.textInput}
          value={primerapellido}
          onChangeText={value => onChange(value, 'primerapellido')}
        />
        <Text style={styles.label}>Segundo apellido:</Text>
        <TextInput
          placeholder="Segundo apellido"
          style={styles.textInput}
          value={segundoapellido}
          onChangeText={value => onChange(value, 'segundoapellido')}
        />
        <Text style={styles.label}>Nombres:</Text>
        <TextInput
          placeholder="Nombre(s)"
          style={styles.textInput}
          value={nombres}
          onChangeText={value => onChange(value, 'nombres')}
        />
        <Text style={styles.label}>Sexo:</Text>
        <Picker
          selectedValue={sexo_genericoid}
          onValueChange={value => onChange(value, 'sexo_genericoid')}>
          <Picker.Item label="Masculino" value={5} key={5} />
          <Picker.Item label="Femenino" value={6} key={6} />
        </Picker>
        <Text style={styles.label}>Correo:</Text>
        <TextInput
          placeholder="Correo"
          style={styles.textInput}
          value={correo_electronico}
          onChangeText={value => onChange(value, 'correo_electronico')}
        />
        <Text style={styles.label}>Celular:</Text>
        <TextInput
          placeholder="Celular"
          style={styles.textInput}
          value={celular}
          onChangeText={value => onChange(value, 'celular')}
        />
        <TextInput
          placeholder="Direccion"
          style={styles.textInput}
          value={direccion}
          onChangeText={value => onChange(value, 'direccion')}
        />
        <Button title="Guardar" onPress={save} color="#003d52" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
    marginTop: 5,
    marginBottom: 15,
  },
});
