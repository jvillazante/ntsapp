import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';
import {EvaluacionesContext} from '../context/EvaluacionesContext';
import {EvaluacionesStackParams} from '../navigator/EvaluacionesNavigator';

interface Props
  extends StackScreenProps<EvaluacionesStackParams, 'EvaluacionesScreen'> {}

export const EvaluacionesScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params;
  const [isRefreshing, setIsRefreshing] = useState(false);
  // const {pacientes, loadPacientes} = useContext(PacientesContext);
  const {evaluaciones, loadEvaluaciones} = useContext(EvaluacionesContext);
  // const {user} = useContext(AuthContext);
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          activeOpacity={0.8}
          style={{marginRight: 10}}
          onPress={() =>
            navigation.navigate('EvaluacionScreen', {paciente_id})
          }>
          <Text>Nueva evaluacion </Text>
        </TouchableOpacity>
      ),
    });
  }, []);

  useEffect(() => {
    loadEvalu();
  }, []);

  const loadEvalu = async () => {
    await loadEvaluaciones(paciente_id);
  };

  const loadEvaluacionesCronicosFromBackend = async () => {
    setIsRefreshing(true);
    await loadEvaluaciones(paciente_id);
    setIsRefreshing(false);
  };

  return (
    <View style={{flex: 1, marginHorizontal: 10}}>
      <FlatList
        data={evaluaciones}
        keyExtractor={p => p.uid}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              navigation.navigate('EvaluacionScreen', {
                uid: item.uid,
              })
            }>
            <View style={styles.container}>
              <Icon name="person-outline" color="gray" size={20} />
              <Text style={styles.pacienteName}>
                {item.fecha_registro}
                {'|'}
                {item.peso} {'|'}
                {item.talla}
                {'|'}
                {item.sistolica}
                {'/'}
                {item.diastolica}
              </Text>
              <View style={{flex: 1}} />
              <Icon name="chevron-forward-outline" color="gray" size={20} />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={loadEvaluacionesCronicosFromBackend}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  pacienteName: {
    marginLeft: 10,
    fontSize: 19,
  },
  itemSeparator: {
    borderBottomWidth: 2,
    marginVertical: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
});
