import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {StackScreenProps} from '@react-navigation/stack';
import {SeguimientosStackParams} from '../navigator/SeguimientosNavigator';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Props
  extends StackScreenProps<SeguimientosStackParams, 'EvaluacionesScreen'> {}

export const SeguimientosOpcionesScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params;
  return (
    // <View style={{flex: 1, marginHorizontal: 10}}>
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.button}
        onPress={() =>
          navigation.navigate('EvaluacionesScreen', {paciente_id})
        }>
        <Text style={styles.buttonText}>Evaluacion clinica</Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.button}
        onPress={() =>
          navigation.navigate('LaboratoriosScreen', {paciente_id})
        }>
        <Text style={styles.buttonText}>Laboratorios</Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.button}
        onPress={() =>
          navigation.navigate('TratamientosScreen', {paciente_id})
        }>
        <Text style={styles.buttonText}>Tratamientos</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  itemSeparator: {
    borderBottomWidth: 2,
    marginVertical: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#003d52',
    padding: 10,

    borderWidth: 2,
    borderColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 100,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
  },
});
