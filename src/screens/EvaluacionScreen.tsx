import React, {useContext, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
  Alert,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';
import {StackScreenProps} from '@react-navigation/stack';
import {useForm} from '../hooks/useForm';
import {EvaluacionesStackParams} from '../navigator/EvaluacionesNavigator';
import {EvaluacionesContext} from '../context/EvaluacionesContext';
import {useTabaquismo} from '../hooks/useTabaquismo';
import {useAlcoholismo} from '../hooks/useAlcoholismo';
import {useExamenOjo} from '../hooks/useExamenOjo';
import {useExamenPie} from '../hooks/useExamenPie';
import {AuthContext} from '../context/AuthContext';

interface Props
  extends StackScreenProps<EvaluacionesStackParams, 'EvaluacionScreen'> {}

export const EvaluacionScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params;
  const {addEvaluacion, loadEvaluaciones, removeError, errorMessage} =
    useContext(EvaluacionesContext);
  const {user} = useContext(AuthContext);
  const {tabaquismos} = useTabaquismo();
  const {alcoholismos} = useAlcoholismo();
  const {examen_ojos} = useExamenOjo();
  const {examen_pies} = useExamenPie();
  const {
    actividad_fisica,
    tabaquismo,
    alcoholismo,
    frutas_verduras,
    sal,
    alimentos_procesados,
    bebidas_azucaradas,
    peso,
    talla,
    imc_valor,
    imc_descripcion,
    cintura,
    sistolica,
    diastolica,
    examen_ojo,
    examen_pie,
    tipo_g,
    // valor,
    // fecha,
    onChange,
    setFormValue,
  } = useForm({
    actividad_fisica: 0,
    tabaquismo: 0,
    alcoholismo: 0,
    frutas_verduras: 0,
    sal: 0,
    alimentos_procesados: 0,
    bebidas_azucaradas: 0,
    peso: 0,
    talla: 0,
    imc_valor: 0,
    imc_descripcion: '',
    cintura: 0,
    sistolica: 0,
    diastolica: 0,
    examen_ojo: 0,
    examen_pie: 0,
    tipo_g: 0,
    // valor: '',
    // fecha: new Date(),
  });
  useEffect(() => {
    if (errorMessage.length === 0) return;
    Alert.alert('Mensaje importante!', errorMessage, [
      {
        text: 'Ok',
        onPress: removeError,
      },
    ]);
  }, [errorMessage]);

  const save = async () => {
    const tempTabaaquismoId = tabaquismo || tabaquismos[0].uid;
    const tempAlcoholismoId = alcoholismo || alcoholismos[0].uid;
    const tempExamenesOjosId = examen_ojo || examen_ojos[0].uid;
    const tempExamenesPieId = examen_pie || examen_pies[0].uid;

    const newEvaluacion = await addEvaluacion(
      paciente_id,
      0,
      actividad_fisica,
      tempTabaaquismoId,
      tempAlcoholismoId,
      talla,
      peso,
      sistolica,
      diastolica,
      tempExamenesOjosId,
      cintura,
      imc_valor,
      imc_descripcion,
      new Date(),
      user?.uid,
      user?.establecimiento_id,
      frutas_verduras,
      sal,
      alimentos_procesados,
      bebidas_azucaradas,
      tempExamenesPieId,
      2,
    );
    console.log('Se guardo correctamente!');
    loadEvaluaciones(paciente_id);
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>
          Durante la semana cuanto tiempo al dia realizo alguna actividad
          fisica:
        </Text>
        <Picker
          selectedValue={actividad_fisica}
          onValueChange={value => onChange(value, 'actividad_fisica')}>
          <Picker.Item label="0" value={0} key={0} />
          <Picker.Item label="15" value={15} key={15} />
          <Picker.Item label="30" value={30} key={30} />
          <Picker.Item label="60" value={60} key={60} />
        </Picker>
        <Text style={styles.label}>Consumo de tabaco:</Text>
        <Picker
          selectedValue={tabaquismo}
          onValueChange={value => onChange(value, 'tabaquismo')}>
          {tabaquismos.map(c => (
            <Picker.Item
              label={c.catalogodescripcion}
              value={c.uid}
              key={c.uid}
            />
          ))}
        </Picker>
        <Text style={styles.label}>Consumo de alcohol:</Text>
        <Picker
          selectedValue={alcoholismo}
          onValueChange={value => onChange(value, 'alcoholismo')}>
          {alcoholismos.map(c => (
            <Picker.Item
              label={c.catalogodescripcion}
              value={c.uid}
              key={c.uid}
            />
          ))}
        </Picker>
        <Text style={styles.label}>
          En la semana consumio fruta y verdura todos los días:
        </Text>
        <Picker
          selectedValue={frutas_verduras}
          onValueChange={value => onChange(value, 'frutas_verduras')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>
          Agrega sal antes o mientras ingiere las comidas
        </Text>
        <Picker
          selectedValue={sal}
          onValueChange={value => onChange(value, 'sal')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>
          Consume alimentos procesado (salchichas, enlatados, etc.)
        </Text>
        <Picker
          selectedValue={alimentos_procesados}
          onValueChange={value => onChange(value, 'alimentos_procesados')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>
          Durante la semana consumió bebidas azucaradas todos los días
        </Text>
        <Picker
          selectedValue={bebidas_azucaradas}
          onValueChange={value => onChange(value, 'bebidas_azucaradas')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>Peso</Text>
        <TextInput
          placeholder="peso"
          style={styles.textInput}
          value={peso}
          onChangeText={value => onChange(value, 'peso')}
        />
        <Text style={styles.label}>Talla</Text>
        <TextInput
          placeholder="talla"
          style={styles.textInput}
          value={talla}
          onChangeText={value => onChange(value, 'talla')}
        />
        <Text style={styles.label}>IMC</Text>
        <TextInput
          placeholder="IMC"
          style={styles.textInput}
          value={imc_valor}
          onChangeText={value => onChange(value, 'imc_valor')}
        />
        <Text style={styles.label}>Circunferencia de cintura</Text>
        <TextInput
          placeholder="Circunferencia de cintura"
          style={styles.textInput}
          value={cintura}
          onChangeText={value => onChange(value, 'cintura')}
        />
        <Text style={styles.label}>Presion Sistólica</Text>
        <TextInput
          placeholder="Presion Sistólica"
          style={styles.textInput}
          value={sistolica}
          onChangeText={value => onChange(value, 'sistolica')}
        />
        <Text style={styles.label}>Presion Diastolica</Text>
        <TextInput
          placeholder="Presion Diastolica"
          style={styles.textInput}
          value={diastolica}
          onChangeText={value => onChange(value, 'diastolica')}
        />
        <Text style={styles.label}>Examen de ojos</Text>
        <Picker
          selectedValue={examen_ojo}
          onValueChange={value => onChange(value, 'examen_ojo')}>
          {examen_ojos.map(c => (
            <Picker.Item
              label={c.catalogodescripcion}
              value={c.uid}
              key={c.uid}
            />
          ))}
        </Picker>

        <Text style={styles.label}>Examen de Pies</Text>
        <Picker
          selectedValue={examen_pie}
          onValueChange={value => onChange(value, 'examen_pie')}>
          {examen_pies.map(c => (
            <Picker.Item
              label={c.catalogodescripcion}
              value={c.uid}
              key={c.uid}
            />
          ))}
        </Picker>
        <Button title="Guardar" onPress={save} color="#003d52" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
    marginTop: 5,
    marginBottom: 15,
  },
});
