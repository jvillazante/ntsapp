import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';

import {StackScreenProps} from '@react-navigation/stack';
import {useForm} from '../hooks/useForm';
import {AuthContext} from '../context/AuthContext';
import {AntecedentesStackParams} from '../navigator/AntecedentesNavigator';
import {AntecedentesContext} from '../context/AntecedentesContext';

interface Props
  extends StackScreenProps<AntecedentesStackParams, 'AntecedenteScreen'> {}

export const AntecedenteScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params; //Id del paciente
  const {user} = useContext(AuthContext);
  const {addAntecedente} = useContext(AntecedentesContext);
  const {
    actividad_fisica,
    tabaquismo,
    alcoholismo,
    talla,
    peso,
    sistolica,
    diastolica,
    examen_ojo,
    cintura,
    imc_valor,
    imc_descripcion,
    frutas_verduras,
    sal,
    alimentos_procesados,
    bebidas_azucaradas,
    examen_pie,
    tipo_g,
    onChange,
    setFormValue,
  } = useForm({
    actividad_fisica: 0,
    tabaquismo: 0,
    alcoholismo: 0,
    talla: 0,
    peso: 0,
    sistolica: 0,
    diastolica: 0,
    examen_ojo: 0,
    cintura: 0,
    imc_valor: 0,
    imc_descripcion: '',
    frutas_verduras: 0,
    sal: 0,
    alimentos_procesados: 0,
    bebidas_azucaradas: 0,
    examen_pie: 0,
    tipo_g: 0,
  });

  const save = async () => {
    const newAntecedente = await addAntecedente(
      paciente_id,
      0,
      actividad_fisica,
      tabaquismo,
      alcoholismo,
      talla,
      peso,
      sistolica,
      diastolica,
      examen_ojo,
      cintura,
      imc_valor,
      imc_descripcion,
      new Date(),
      user?.uid,
      user?.establecimiento_id,
      frutas_verduras,
      sal,
      alimentos_procesados,
      bebidas_azucaradas,
      examen_pie,
      2,
    );
    // console.log(newAntecedente);
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>
          Durante la semana cuanto tiempo al dia realizo alguna actividad
          fisica:
        </Text>
        <Picker
          selectedValue={alcoholismo}
          onValueChange={value => onChange(value, 'alcoholismo')}>
          <Picker.Item label="0" value={0} key={0} />
          <Picker.Item label="15" value={15} key={15} />
          <Picker.Item label="30" value={30} key={30} />
          <Picker.Item label="60" value={60} key={60} />
          <Picker.Item label="+60" value={+60} key={+60} />
        </Picker>
        <Text style={styles.label}>Consumo de tabaco:</Text>
        <Picker
          selectedValue={tabaquismo}
          onValueChange={value => onChange(value, 'tabaquismo')}>
          <Picker.Item
            label="Si Consumo de Tabaco (Sin Humo)"
            value={1}
            key={1}
          />
          <Picker.Item label="Si Consumo de Tabaco" value={2} key={2} />
          <Picker.Item label="No Consumo de Tabaco" value={3} key={3} />
        </Picker>
        <Text style={styles.label}>Consumo de alcohol:</Text>
        <Picker
          selectedValue={alcoholismo}
          onValueChange={value => onChange(value, 'alcoholismo')}>
          <Picker.Item label="No" value={1} key={1} />
          <Picker.Item label="Ocasional" value={2} key={2} />
          <Picker.Item label="Diario" value={3} key={3} />
          <Picker.Item label="Nocivo" value={4} key={4} />
        </Picker>
        <Text style={styles.label}>
          En la semana consumio fruta y verdura todos los días:
        </Text>
        <Picker
          selectedValue={frutas_verduras}
          onValueChange={value => onChange(value, 'frutas_verduras')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>
          Agrega sal antes o mientras ingiere las comidas
        </Text>
        <Picker
          selectedValue={sal}
          onValueChange={value => onChange(value, 'sal')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>
          Consume alimentos procesado (salchichas, enlatados, etc.)
        </Text>
        <Picker
          selectedValue={alimentos_procesados}
          onValueChange={value => onChange(value, 'alimentos_procesados')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>
          Durante la semana consumió bebidas azucaradas todos los días
        </Text>
        <Picker
          selectedValue={bebidas_azucaradas}
          onValueChange={value => onChange(value, 'bebidas_azucaradas')}>
          <Picker.Item label="Si" value={1} key={1} />
          <Picker.Item label="No" value={0} key={0} />
        </Picker>
        <Text style={styles.label}>Peso</Text>
        <TextInput
          placeholder="peso"
          style={styles.textInput}
          value={peso}
          onChangeText={value => onChange(value, 'peso')}
        />
        <Text style={styles.label}>Talla</Text>
        <TextInput
          placeholder="talla"
          style={styles.textInput}
          value={talla}
          onChangeText={value => onChange(value, 'talla')}
        />
        <Text style={styles.label}>IMC</Text>
        <TextInput
          placeholder="IMC"
          style={styles.textInput}
          value={imc_valor}
          onChangeText={value => onChange(value, 'imc_valor')}
        />
        <Text style={styles.label}>Circunferencia de cintura</Text>
        <TextInput
          placeholder="Circunferencia de cintura"
          style={styles.textInput}
          value={cintura}
          onChangeText={value => onChange(value, 'cintura')}
        />
        <Text style={styles.label}>Presion Sistólica</Text>
        <TextInput
          placeholder="Presion Sistólica"
          style={styles.textInput}
          value={sistolica}
          onChangeText={value => onChange(value, 'sistolica')}
        />
        <Text style={styles.label}>Presion Diastolica</Text>
        <TextInput
          placeholder="Presion Diastolica"
          style={styles.textInput}
          value={diastolica}
          onChangeText={value => onChange(value, 'diastolica')}
        />
        <Text style={styles.label}>Examen de ojos</Text>
        <Picker
          selectedValue={examen_ojo}
          onValueChange={value => onChange(value, 'examen_ojo')}>
          <Picker.Item label="Normal" value={1} key={1} />
          <Picker.Item label="Anormal" value={2} key={2} />
          <Picker.Item label="Nunca Hecho" value={3} key={3} />
        </Picker>
        <Text style={styles.label}>Examen de Pies</Text>
        <Picker
          selectedValue={examen_pie}
          onValueChange={value => onChange(value, 'examen_pie')}>
          <Picker.Item label="Normal" value={1} key={1} />
          <Picker.Item label="Anormal" value={2} key={2} />
          <Picker.Item label="Nunca Hecho" value={3} key={3} />
        </Picker>
        <Button title="Guardar" onPress={save} color="#003d52" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
    marginTop: 5,
    marginBottom: 15,
  },
});
