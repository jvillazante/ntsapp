import React, {useContext, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Button,
  Alert,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';
import {StackScreenProps} from '@react-navigation/stack';
import {useForm} from '../hooks/useForm';
import {LaboratoriosStackParams} from '../navigator/LaboratoriosNavigator';
import {useLaboratorios} from '../hooks/useLaboratorios';
// import {AuthContext} from '../context/AuthContext';
import {LaboratoriosContext} from '../context/LaboratoriosContext';
import {AuthContext} from '../context/AuthContext';

interface Props
  extends StackScreenProps<LaboratoriosStackParams, 'LaboratorioScreen'> {}

export const LaboratorioScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params;
  const {user} = useContext(AuthContext);
  const {addLaboratorio, loadLaboratorios, removeError, errorMessage} =
    useContext(LaboratoriosContext);
  const {laboratorios} = useLaboratorios();
  const {laboratorio_id, valor, fecha, onChange, setFormValue} = useForm({
    laboratorio_id: 0,
    valor: '',
    fecha: new Date(),
  });
  useEffect(() => {
    if (errorMessage.length === 0) return;

    Alert.alert('Mensaje importante!', errorMessage, [
      {
        text: 'Ok',
        onPress: removeError,
      },
    ]);
  }, [errorMessage]);

  const save = async () => {
    const tempLaboratorioId = laboratorio_id || laboratorios[0].uid;
    // console.log(paciente_id, 0, tempLaboratorioId, valor, fecha);
    const newLaboratorio = await addLaboratorio(
      paciente_id,
      0,
      tempLaboratorioId,
      valor,
      fecha,
      user?.uid,
      2,
    );
    // console.log(paciente_id, 0, tempLaboratorioId, valor, fecha, user?.uid, 2);

    // console.log('Se guardo correctamente!');
    loadLaboratorios(paciente_id);
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.label}>Laboratorio</Text>
        <Picker
          selectedValue={laboratorio_id}
          onValueChange={value => onChange(value, 'laboratorio_id')}>
          {laboratorios.map(c => (
            <Picker.Item label={c.descripcion} value={c.uid} key={c.uid} />
          ))}
        </Picker>
        <Text style={styles.label}>Valor:</Text>
        <TextInput
          placeholder="valor"
          style={styles.textInput}
          value={valor}
          onChangeText={value => onChange(value, 'valor')}
        />
        <Text style={styles.label}>Fecha registro:</Text>
        <TextInput
          placeholder="Fecha"
          style={styles.textInput}
          value={fecha}
          onChangeText={value => onChange(value, 'fecha')}
        />

        <Button title="Guardar" onPress={save} color="#003d52" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 45,
    marginTop: 5,
    marginBottom: 15,
  },
});
