import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';
import {PacientesContext} from '../context/PacientesContext';
import {AuthContext} from '../context/AuthContext';
import {AntecedentesStackParams} from '../navigator/AntecedentesNavigator';

interface Props
  extends StackScreenProps<AntecedentesStackParams, 'AntecedentesScreen'> {}

export const AntecedentesScreen = ({navigation}: Props) => {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const {pacientes, loadPacientes} = useContext(PacientesContext);
  const {user} = useContext(AuthContext);

  // useEffect(() => {
  //   navigation.setOptions({
  //     headerRight: () => (
  //       <TouchableOpacity
  //         activeOpacity={0.8}
  //         style={{marginRight: 10}}
  //         onPress={() => navigation.navigate('PacienteScreen', {})}>
  //         <Text>Nuevo Paciente </Text>
  //       </TouchableOpacity>
  //     ),
  //   });
  // }, []);

  const loadPacientesFromBackend = async () => {
    setIsRefreshing(true);
    await loadPacientes(
      user?.departamento_id,
      user?.red_id,
      user?.municipio_id,
      user?.establecimiento_id,
    );
    setIsRefreshing(false);
  };

  return (
    <View style={{flex: 1, marginHorizontal: 10}}>
      <FlatList
        data={pacientes}
        keyExtractor={p => p.uid}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              navigation.navigate('AntecedenteScreen', {
                paciente_id: item.uid,
              })
            }>
            <View style={styles.container}>
              <Icon name="person-outline" color="gray" size={20} />
              <Text style={styles.pacienteName}>
                {item.nombres} {item.primerapellido} {item.segundoapellido}
              </Text>
              <View style={{flex: 1}} />
              <Icon name="chevron-forward-outline" color="gray" size={20} />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={loadPacientesFromBackend}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  pacienteName: {
    marginLeft: 10,
    fontSize: 19,
  },
  itemSeparator: {
    borderBottomWidth: 2,
    marginVertical: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
});
