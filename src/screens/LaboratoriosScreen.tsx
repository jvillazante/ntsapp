import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';
import {LaboratoriosStackParams} from '../navigator/LaboratoriosNavigator';
import {LaboratoriosContext} from '../context/LaboratoriosContext';

interface Props
  extends StackScreenProps<LaboratoriosStackParams, 'LaboratoriosScreen'> {}

export const LaboratoriosScreen = ({navigation, route}: Props) => {
  const {paciente_id = 0} = route.params;
  const [isRefreshing, setIsRefreshing] = useState(false);
  // const {pacientes, loadPacientes} = useContext(PacientesContext);
  const {laboratorioscronicos, loadLaboratorios} =
    useContext(LaboratoriosContext);
  // const {user} = useContext(AuthContext);
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          activeOpacity={0.8}
          style={{marginRight: 10}}
          onPress={() =>
            navigation.navigate('LaboratorioScreen', {paciente_id})
          }>
          <Text>Nuevo laboratorio </Text>
        </TouchableOpacity>
      ),
    });
  }, []);

  useEffect(() => {
    loadLabos();
  }, []);

  const loadLabos = async () => {
    await loadLaboratorios(paciente_id);
  };

  const loadLaboratoriosCronicosFromBackend = async () => {
    setIsRefreshing(true);
    await loadLaboratorios(paciente_id);
    setIsRefreshing(false);
  };

  return (
    <View style={{flex: 1, marginHorizontal: 10}}>
      <FlatList
        data={laboratorioscronicos}
        keyExtractor={p => p.uid}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              navigation.navigate('LaboratorioScreen', {
                uid: item.uid,
              })
            }>
            <View style={styles.container}>
              <Icon name="person-outline" color="gray" size={20} />
              <Text style={styles.pacienteName}>
                {item.fecha_registro} {'|'}
                {item.valor}
                {'|'}
                {item.laboratorio.descripcion}
                {'|'}
                {item.laboratorio.unidad}
              </Text>
              <View style={{flex: 1}} />
              <Icon name="chevron-forward-outline" color="gray" size={20} />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={loadLaboratoriosCronicosFromBackend}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  pacienteName: {
    marginLeft: 10,
    fontSize: 19,
  },
  itemSeparator: {
    borderBottomWidth: 2,
    marginVertical: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
});
