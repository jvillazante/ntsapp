import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {StackScreenProps} from '@react-navigation/stack';
import {DiagnosticosStackParams} from '../navigator/DiagnosticosNavigator';
import {DiagnosticosContext} from '../context/DiagnosticosContext';

// import {PacientesContext} from '../context/PacientesContext';
// import {PacientesStackParams} from '../navigator/PacientesNavigator';

interface Props
  extends StackScreenProps<DiagnosticosStackParams, 'DiagnosticosScreen'> {}

export const DiagnosticosScreen = ({navigation}: Props) => {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const {diagnosticos, loadDiagnosticos} = useContext(DiagnosticosContext);

  // useEffect(() => {
  //   navigation.setOptions({
  //     headerRight: () => (
  //       <TouchableOpacity
  //         activeOpacity={0.8}
  //         style={{marginRight: 10}}
  //         onPress={() => navigation.navigate('DiagnosticoScreen', {id: 0})}>
  //         <Text>Agregar </Text>
  //       </TouchableOpacity>
  //     ),
  //   });
  // }, []);

  const loadDiagnosticosFromBackend = async () => {
    setIsRefreshing(true);
    await loadDiagnosticos();
    setIsRefreshing(false);
  };

  return (
    <View style={{flex: 1, marginHorizontal: 10}}>
      <FlatList
        data={diagnosticos}
        keyExtractor={p => p.uid}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              navigation.navigate('DiagnosticoScreen', {id: item.uid})
            }>
            <View style={styles.container}>
              <Icon name="person-outline" color="gray" size={20} />
              <Text style={styles.diagnosticoName}>
                {item.uid} {item.diagnostico_id} {item.fecha_registro}
              </Text>
              <View style={{flex: 1}} />
              <Icon name="chevron-forward-outline" color="gray" size={20} />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={loadDiagnosticosFromBackend}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  diagnosticoName: {
    marginLeft: 10,
    fontSize: 19,
  },
  itemSeparator: {
    borderBottomWidth: 2,
    marginVertical: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
});
