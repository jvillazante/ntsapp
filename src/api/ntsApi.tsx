import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

// const baseURL = 'http://192.168.0.3:3000/api';
const baseURL = 'http://consultaexternados.minsalud.gob.bo/api';

const ntsApi = axios.create({baseURL});

ntsApi.interceptors.request.use(async config => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    config.headers['x-token'] = token;
  }
  return config;
});

export default ntsApi;
