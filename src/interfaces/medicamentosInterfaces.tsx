// Generated by https://quicktype.io

export interface MedicamentosResponse {
  medicamentos: Medicamento[];
}

export interface Medicamento {
  id: number;
  codificacion: string;
  med_comercial: string;
  tipo: Tipo;
  med_unidad: string;
  med_concentracion: string;
  med_generico: string;
  estado_id: number;
}

export enum Tipo {
  F = 'F',
}
