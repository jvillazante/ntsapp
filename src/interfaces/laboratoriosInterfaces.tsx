// Generated by https://quicktype.io

export interface LaboratoriosCronicosResponse {
  total: number;
  laboratoriosCronicos: LaboratoriosCronico[];
}

export interface LaboratoriosCronico {
  paciente_cronico_id: number;
  persona_id: number;
  valor: number;
  fecha_registro: string;
  laboratorio: Laboratorio;
  uid: number;
}

// export interface Laboratorio {
//   descripcion: string;
//   unidad: string;
//   tipo_descripcion: string;
//   minimo: number;
//   maximo: number;
// }

// Generated by https://quicktype.io

export interface LaboratoriosResponse {
  laboratorios: Laboratorio[];
}

export interface Laboratorio {
  descripcion: string;
  unidad: string;
  tipo: number;
  tipo_descripcion: string;
  minimo: number;
  maximo: number;
  uid?: number;
}
